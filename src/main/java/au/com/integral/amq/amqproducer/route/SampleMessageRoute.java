package au.com.integral.amq.amqproducer.route;

import au.com.integral.amq.amqproducer.model.SampleMessage;
import au.com.integral.amq.amqproducer.model.SampleResponse;
import org.apache.camel.ExchangePattern;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.springframework.stereotype.Component;

@Component
public class SampleMessageRoute extends RouteBuilder {

    // JSON Data Format
    JacksonDataFormat sampleMessageFormat = new JacksonDataFormat(SampleMessage.class);

    // JSON Data Format
    JacksonDataFormat jsonDataFormat = new JacksonDataFormat(SampleResponse.class);

    @Override
    public void configure() throws Exception {

        from("direct:requestMessage").routeId("producerRoute")
                .log("Request: ${body}")
                .marshal(sampleMessageFormat)
                .to("activemq:queue:sendMessageQueue?exchangePattern=InOut").id("sendQueue")
                .unmarshal(jsonDataFormat)
                .log("Response: ${body}")
                .end();
    }
}
