package au.com.integral.amq.amqproducer;

import au.com.integral.amq.amqproducer.model.SampleResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.CamelContext;
import org.apache.camel.EndpointInject;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.AdviceWith;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.spring.junit5.CamelSpringBootTest;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.test.annotation.DirtiesContext;

import java.nio.charset.Charset;

import static org.apache.camel.language.constant.ConstantLanguage.constant;

@Slf4j
@CamelSpringBootTest
@EnableAutoConfiguration
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class AmqProducerApplicationTests {

	@Test
	void contextLoads() {
	}

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate testRestTemplate;

	@Autowired
	ProducerTemplate producerTemplate;

	@Autowired
	private CamelContext context;

	@EndpointInject("mock:test")
	MockEndpoint mockEndpoint;

	@Value("classpath:testMessage.json")
	Resource testPayload;

	@Value("classpath:sampleResponse.json")
	Resource testResponse;

	String sampleRequest;
	String sampleResponse;

	@BeforeEach
	public void initRoute() throws Exception {

		log.info("Initiating sample request and response...");

		sampleRequest = FileUtils.readFileToString(testPayload.getFile(), Charset.defaultCharset());

		sampleResponse = FileUtils.readFileToString(testResponse.getFile(), Charset.defaultCharset());

//		RouteBuilder postProcessor = new RouteBuilder() {
//			@Override
//			public void configure() throws Exception {
//				from("direct:postProcessor")
//						.setBody(constant(response))
//						.end();
//			}
//		};

//		AdviceWith.adviceWith(context,"producerRoute",
//				a -> a.interceptSendToEndpoint("sendQueue")
//						.to(mockEndpoint)
//						.skipSendToOriginalEndpoint()
//						.afterUri("direct:postProcessor"));

		log.info("initiating adviceWith routes...");
		AdviceWith.adviceWith(context,
				"producerRoute",
				a -> a.weaveById("sendQueue")
						.replace()
						.to(mockEndpoint)
						.setBody(constant(sampleResponse))
		);

//		AdviceWith.adviceWith(context,
//				"producerRoute",
//				a -> a.weaveAddLast().to(mockEndpoint)
//		);
	}

	@Test
	public void sendProducerTest() {
		log.info("Sample Request: {}", sampleRequest);
		producerTemplate.sendBody("direct:requestMessage", sampleRequest);
		mockEndpoint.expectedBodyReceived();
		mockEndpoint.getExchanges();
		String sampleMessage = mockEndpoint.getExchanges().get(0).getIn().getBody(String.class);
		log.info("Test result: {}", sampleMessage);
	}

	@Test
	public void sendMessageAPITest() {

		HttpEntity<String> requestEntity = new HttpEntity<>(sampleRequest, generateHeaders());

		String uri = "http://localhost:" + port + "/sendMessage";

		ResponseEntity<SampleResponse> response = this.testRestTemplate.exchange(uri, HttpMethod.POST, requestEntity,
				SampleResponse.class);
		Assertions.assertNotNull(response.getBody().getResponse());
		Assertions.assertEquals(response.getBody().getResponse(),"test response");
	}

	private HttpHeaders generateHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("content-type", "application/json");

		return headers;
	}
}
