package au.com.integral.amq.amqproducer.controller;

import au.com.integral.amq.amqproducer.model.SampleMessage;
import au.com.integral.amq.amqproducer.model.SampleResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.CamelContext;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class AMQController {

    @Autowired
    private ProducerTemplate producerTemplate;

    @Autowired
    private CamelContext camelContext;

    @PostMapping(value = "/sendMessage", consumes = "application/json", produces = "application/json")
    public SampleResponse sendMessage(@RequestBody SampleMessage message) throws Exception {

        log.info("Incoming Request: {}", message);
        return producerTemplate.requestBody("direct:requestMessage",
                message,
                SampleResponse.class);
    }

    @GetMapping(value = "/hello", produces = "application/json")
    public String hello() {
        return "{ \"body\": \"hello world\" }";
    }
}
