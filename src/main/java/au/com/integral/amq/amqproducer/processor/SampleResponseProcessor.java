package au.com.integral.amq.amqproducer.processor;

import au.com.integral.amq.amqproducer.model.SampleResponse;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;


@Component
public class SampleResponseProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        SampleResponse sampleResponse = exchange.getIn().getBody(SampleResponse.class);

    }
}
